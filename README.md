nick-gnudalf
============

based on the original nick app, but it also
- has random background colors
- makes noise when pressing pedals
- changes font when bottom pedals are pressed
- changes font color when top pedals are pressed
