from st3m.application import Application, ApplicationContext
from st3m.ui.colours import PUSH_RED, GO_GREEN, BLACK
from st3m.goose import Dict, Any
from st3m.input import InputState
from st3m import logging
from st3m.reactor import Responder
from ctx import Context
import leds

import json
import math
import random
import st3m.run

log = logging.Log(__name__, level=logging.INFO)
log.info("gnudalf nick")

import bl00mbox
blm = bl00mbox.Channel("nick-gnudalf")
synth = bl00mbox.patches.tinysynth_fm
tiny = blm.new(synth)
tiny.signals.output = blm.mixer
tiny.signals.sustain = 1

class Configuration:
    def __init__(self) -> None:
        self.name = "flow3r"
        self.size: int = 75
        self.font: int = 5

    @classmethod
    def load(cls, path: str) -> "Configuration":
        res = cls()
        try:
            with open(path) as f:
                jsondata = f.read()
            data = json.loads(jsondata)
        except OSError:
            data = {}
        if "name" in data and type(data["name"]) == str:
            res.name = data["name"]
        if "size" in data:
            if type(data["size"]) == float:
                res.size = int(data["size"])
            if type(data["size"]) == int:
                res.size = data["size"]
        if "font" in data and type(data["font"]) == int:
            res.font = data["font"]
        return res

    def save(self, path: str) -> None:
        d = {
            "name": self.name,
            "size": self.size,
            "font": self.font,
        }
        jsondata = json.dumps(d)
        with open(path, "w") as f:
            f.write(jsondata)
            f.close()


class GnudalfNickApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._scale = 1.0
        self._led = 0.0
        self._phase = 0.0
        self._filename = "/flash/nick.json"
        self._color = (0,0,0)
        self._font = 0
        self._bg_color = (0,0,0)
        self._bg_alpha = 1.0
        self.new_color()
        self._config = Configuration.load(self._filename)

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = self._config.size
        ctx.font = ctx.get_font_name(self._font if self._font != 7 else self._config.font)

        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.rgba(*self._bg_color, self._bg_alpha).rectangle(-120, -120, 240, 240).fill()

        ctx.rgb(*self._color)
        ctx.move_to(0, 0)
        ctx.save()
        ctx.scale(self._scale, 1)
        ctx.text(self._config.name)
        ctx.restore()

        leds.set_hsv(int(self._led), abs(self._scale) * 360, 1, 0.2)

        leds.update()

    def on_exit(self) -> None:
        self._config.save(self._filename)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        global tiny

        self._phase += delta_ms / 1000
        self._scale = math.sin(self._phase)
        self._led += delta_ms / 45
        if self._led >= 40:
            self._led = 0

        if self._bg_alpha > 0:
            self._bg_alpha -= delta_ms / 3000
        if self._bg_alpha <= 0:
            self.new_bg_color()

        petals = self.input.captouch.petals
        for i in range (0, 10):
            if petals[i].whole.pressed:
                self.trigger_button()
                if i < 5:
                    self._scale = random.random()
                if i % 2 == 0:
                    self.new_color()
                else:
                    self._font = random.getrandbits(3)
                break

    def trigger_button(self):
        self.new_bg_color()
        tiny.signals.pitch.freq = 200 + int(random.random() * 2000)
        tiny.signals.trigger.start()
        log.debug(f"freq: { tiny.signals.pitch.freq }")

    def new_bg_color(self):
        self._bg_color = (random.random(), random.random(), random.random())
        self._bg_alpha = 1

    def new_color(self):
        self._color = (random.random(), random.random(), random.random())

if __name__ == '__main__':
    st3m.run.run_view(GnudalfNickApp(ApplicationContext()))
